class Exercise < ActiveRecord::Base
  has_many :phases, dependent: :destroy, autosave: true
  accepts_nested_attributes_for :phases, allow_destroy: true, reject_if: proc { |phases| phases['action'].blank?}
end
