# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ($) ->
  $(document).ready ->
    if $('.duplicatable-form').length
      nestedForm = $('.duplicatable-form').last().clone()

      $('.duplicate-form').click (e) ->
        e.preventDefault()

        lastNestedForm = $('.duplicatable-form').last()
        newNestedForm  = $(nestedForm).clone()
        formsOnPage    = $('.duplicatable-form').length

        $(newNestedForm).find('label').each ->
          oldLabel = $(this).attr 'for'
          newLabel = oldLabel.replace(new RegExp(/_[0-9]+_/), "_#{formsOnPage}_")
          $(this).attr 'for', newLabel

        $(newNestedForm).find('select, input').each ->
          oldId = $(this).attr 'id'
          newId = oldId.replace(new RegExp(/_[0-9]+_/), "_#{formsOnPage}_")
          $(this).attr 'id', newId

          oldName = $(this).attr 'name'
          newName = oldName.replace(new RegExp(/\[[0-9]+\]/), "[#{formsOnPage}]")
          $(this).attr 'name', newName

        $( newNestedForm ).insertAfter( lastNestedForm )

      $('body').on 'click', '.dupli-destroy', (e) ->
        e.preventDefault()
        $(this).closest('.duplicatable-form').slideUp().remove()
