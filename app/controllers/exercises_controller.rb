class ExercisesController < ApplicationController
  def index
    @exercise = Exercise.new
    @phase = @exercise.phases.build

    @daysofweek = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    @monday = Exercise.includes(:phases).where("weekday = 1")
    @tuesday = Exercise.includes(:phases).where("weekday = 2")
    @wednesday = Exercise.includes(:phases).where("weekday = 3")
    @thursday = Exercise.includes(:phases).where("weekday = 4")
    @friday = Exercise.includes(:phases).where("weekday = 5")
    @saturday = Exercise.includes(:phases).where("weekday = 6")
    @sunday = Exercise.includes(:phases).where("weekday = 7")
    @data = []
    @data.push(@monday).push(@tuesday).push(@wednesday).push(@thursday).push(@friday).push(@saturday).push(@sunday)
  end
  def show
    @exercise = Exercise.find(params[:id])
  end
  def new
    @exercise = Exercise.new
    @phase = @exercise.phases.build
  end
  def edit
    @exercise = Exercise.find(params[:id])
  end
  def destroy
    @exercise = Exercise.find(params[:id])
    @exercise.destroy
    redirect_to action: "index"
  end
  def create
    @exercise = Exercise.new(exercise_params)
    @exercise.save
     redirect_to action: "index"
  end
  def update
    @exercise = Exercise.find(params[:id])
    if @exercise.update(exercise_params)
      redirect_to action: "index"
    else
      render 'edit'
    end
  end

  private
    def exercise_params
      params.fetch(:exercise, {}).permit(:weekday, :description, phases_attributes: [:id, :action])
    end
end
