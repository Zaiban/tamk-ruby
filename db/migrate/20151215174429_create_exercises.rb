class CreateExercises < ActiveRecord::Migration
  def change
    create_table :exercises do |t|
      t.integer :weekday
      t.text :description

      t.timestamps null: false
    end
    create_table :phases do |t|
      t.belongs_to :exercise, index:true
      t.text :action

      t.timestamps null: false
    end
  end
end
